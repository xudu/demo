#!/bin/bash
COMPOSE="http://download.eng.pek2.redhat.com/rhel-9/nightly/RHEL-9/latest-RHEL-9.5.0/compose"
NETPERF="http://netqe-bj.usersys.redhat.com/share/tools/netperf-20210121.tar.bz2"
DPDK_RPM="http://download.devel.redhat.com/brewroot/packages/dpdk/20.11/3.el8/x86_64/dpdk-20.11-3.el8.x86_64.rpm"
DPDK_TOOLS_RPM="$(dirname "$DPDK_RPM")/$(basename "$DPDK_RPM"| sed 's/dpdk/dpdk-tools/')"
DRIVECTL_RPM="http://download.devel.redhat.com/brewroot/packages/driverctl/0.95/4.el8/noarch/driverctl-0.95-4.el8.noarch.rpm"

# repo
cat <<-EOF >/etc/yum.repos.d/beaker-harness.repo
	[beaker-harness]
	name=beaker-harness
	baseurl=http://beaker.engineering.redhat.com/harness/RedHatEnterpriseLinux9/
	enabled=1
	gpgcheck=0
EOF
cat <<-EOF >/etc/yum.repos.d/beaker-BaseOS-debuginfo.repo
	[beaker-BaseOS-debuginfo]
	name=beaker-BaseOS-debuginfo
	baseurl=$COMPOSE/BaseOS/\$basearch/debug/tree/
	enabled=1
	gpgcheck=0
	skip_if_unavailable=1
EOF
cat <<-EOF >/etc/yum.repos.d/beaker-AppStream-debuginfo.repo
	[beaker-AppStream-debuginfo]
	name=beaker-AppStream-debuginfo
	baseurl=$COMPOSE/AppStream/\$basearch/debug/tree/
	enabled=1
	gpgcheck=0
	skip_if_unavailable=1
EOF
cat <<-EOF >/etc/yum.repos.d/beaker-HighAvailability-debuginfo.repo
	[beaker-HighAvailability-debuginfo]
	name=beaker-HighAvailability-debuginfo
	baseurl=$COMPOSE/HighAvailability/\$basearch/debug/tree/
	enabled=1
	gpgcheck=0
	skip_if_unavailable=1
EOF
cat <<-EOF >/etc/yum.repos.d/beaker-ResilientStorage-debuginfo.repo
	[beaker-ResilientStorage-debuginfo]
	name=beaker-ResilientStorage-debuginfo
	baseurl=$COMPOSE/ResilientStorage/\$basearch/debug/tree/
	enabled=1
	gpgcheck=0
	skip_if_unavailable=1
EOF
cat <<-EOF >/etc/yum.repos.d/beaker-NFV-debuginfo.repo
	[beaker-NFV-debuginfo]
	name=beaker-NFV-debuginfo
	baseurl=$COMPOSE/NFV/\$basearch/debug/tree/
	enabled=1
	gpgcheck=0
	skip_if_unavailable=1
EOF
cat <<-EOF >/etc/yum.repos.d/beaker-RT-debuginfo.repo
	[beaker-RT-debuginfo]
	name=beaker-RT-debuginfo
	baseurl=$COMPOSE/RT/\$basearch/debug/tree/
	enabled=1
	gpgcheck=0
	skip_if_unavailable=1
EOF
cat <<-EOF >/etc/yum.repos.d/beaker-BaseOS.repo
	[beaker-BaseOS]
	name=beaker-BaseOS
	baseurl=$COMPOSE/BaseOS/\$basearch/os/
	enabled=1
	gpgcheck=0
	skip_if_unavailable=1
EOF
cat <<-EOF >/etc/yum.repos.d/beaker-AppStream.repo
	[beaker-AppStream]
	name=beaker-AppStream
	baseurl=$COMPOSE/AppStream/\$basearch/os/
	enabled=1
	gpgcheck=0
	skip_if_unavailable=1
EOF
cat <<-EOF >/etc/yum.repos.d/beaker-HighAvailability.repo
	[beaker-HighAvailability]
	name=beaker-HighAvailability
	baseurl=$COMPOSE/HighAvailability/\$basearch/os/
	enabled=1
	gpgcheck=0
	skip_if_unavailable=1
EOF
cat <<-EOF >/etc/yum.repos.d/beaker-ResilientStorage.repo
	[beaker-ResilientStorage]
	name=beaker-ResilientStorage
	baseurl=$COMPOSE/ResilientStorage/\$basearch/os/
	enabled=1
	gpgcheck=0
	skip_if_unavailable=1
EOF
cat <<-EOF >/etc/yum.repos.d/beaker-NFV.repo
	[beaker-NFV]
	name=beaker-NFV
	baseurl=$COMPOSE/NFV/\$basearch/os/
	enabled=1
	gpgcheck=0
	skip_if_unavailable=1
EOF
cat <<-EOF >/etc/yum.repos.d/beaker-RT.repo
	[beaker-RT]
	name=beaker-RT
	baseurl=$COMPOSE/RT/\$basearch/os/
	enabled=1
	gpgcheck=0
	skip_if_unavailable=1
EOF
cat <<-EOF >/etc/yum.repos.d/beaker-CRB.repo
	[beaker-CRB]
	name=beaker-CRB
	baseurl=$COMPOSE/CRB/\$basearch/os/
	enabled=1
	gpgcheck=0
	skip_if_unavailable=1
EOF

# repo installing
dnf install -y iproute iputils ethtool procps-ng wget traceroute tcpdump socat rsync nmap nmap-ncat net-tools nc
dnf install -y git gcc vim automake bzip2 expect bind-utils
dnf install -y beakerlib-redhat
dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm
dnf install -y https://rpmfind.net/linux/fedora/linux/development/rawhide/Everything/x86_64/os/Packages/r/rpmrebuild-2.17-6.fc41.noarch.rpm

# compile installing
if ! which netperf >/dev/null; then
	wget -nv -N "$NETPERF"
	tar xjvf "$(basename "$NETPERF")"
	pushd "$(basename "$NETPERF" | awk -F. '{print $1}')" || echo "pushd error" >> "output"
	./autogen.sh
	./configure CFLAGS=-fcommon && make && make install
	popd || echo "pushd error" >> "output"
	netperf -V
fi

if ! which dpdk >/dev/null; then
	dnf install -y "$DPDK_RPM" "$DPDK_TOOLS_RPM" "$DRIVECTL_RPM"
	which dpdk-testpmd &>/dev/null || ln -s "$(which testpmd)" /usr/bin/dpdk-testpmd
fi

# download files
ADD http://lab-02.rhts.eng.rdu.redhat.com/beaker/anamon3 /usr/local/sbin/anamon