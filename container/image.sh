#!/bin/bash
QUAY=docker://quay.io/rh-ee-xudu/rhel
NFS_SERVER=netqe-bj.usersys.redhat.com:/home/share/xudu/oc_image

UPLOAD=no
ARCHIVE=no
MAINTAINER=elondu
VERSION=0.1
CONTAINERFILE=yes

# Functions
cleanup()
{
 	#podman images | grep "$OUTPUT_IMAGE" | awk '{print $3}' | xargs -I {} bash -c "podman rm {}; podman rmi {}"
	grep -qs '/mnt/container' /proc/mounts && umount /mnt/container
}

create_container()
{
	local image=
	local tar_image=
	local remote_image=
	local local_image="localhost/$NEW_IMAGE"

 	podman images | grep "$NEW_IMAGE" | awk '{print $3}' | xargs -I {} bash -c "podman rm -f {}; podman rmi -f {}"

	if [ "$CONTAINERFILE" = "yes" ]; then
		cat > Containerfile <<-EOF
			FROM "$BASE_IMAGE"
			COPY $INSTALL_FILE /opt
			RUN sh /opt/$INSTALL_FILE 2>&1 | tee /tmp/install.log
			LABEL maintainer="$MAINTAINER"
			LABEL version="$VERSION"
			WORKDIR /root/
			CMD ["/bin/bash"]
		EOF
		buildah bud -t "$local_image" .
	else
		image=$(buildah from "$BASE_IMAGE")
		buildah copy "$image" "$INSTALL_FILE" "/opt"
		buildah run "$image" -- bash -c "sh /opt/$INSTALL_FILE 2>&1 | tee /tmp/install.log"
		sleep 3
		buildah config --label maintainer="$MAINTAINER" "$image"
		buildah config --label version="$VERSION" "$image"
		buildah commit "$image" "$local_image"
	fi

	if [[ $ARCHIVE = "yes" ]] ; then
		tar_image="${NEW_IMAGE//:/-}".tar
		podman save -o "$tar_image" "$local_image"
		echo "archive $tar_image to $NFS_SERVER"
		mkdir -p /mnt/container
		mount "$NFS_SERVER" /mnt/container
		cp "$tar_image" /mnt/container
		ls -l /mnt/container
		umount /mnt/container
	fi

	if [[ $UPLOAD = "yes" ]] ; then
		remote_image="$QUAY"/"$NEW_IMAGE"
		echo "uploading $local_image to $remote_image"
		buildah login -u="rh-ee-xudu" -p="7U1Ik4haUvzwo+MLZUAEysElsS1q/VuuNUJUjYj7vaOmds9K8lwP5J1WYsgv9pYq" quay.io
		sleep 3
		buildah push "$local_image" "$remote_image"
	fi
}

image_actions()
{
	case "$ACTION" in
		create)
			if [ -z "$BASE_IMAGE" ]; then
				echo "Error: Don't specify based image!"
				return 1
			fi
			if [ -z "$NEW_IMAGE" ]; then
				echo "Error: Don't specify new image name!"
				return 1
			fi

			echo "Create $NEW_IMAGE based on $BASE_IMAGE"
			rpm -qa | grep -q podman || yum install -y podman
			rpm -qa | grep -q buildah || yum install -y buildah

			if [ -z "$INSTALL_FILE" ]; then
				INSTALL_FILE="./install.sh"
			fi

			create_container
			;;
		*)
			echo "Error: Could not parse action $ACTION!"
			exit 1
			;;
	esac
}

trap "cleanup" EXIT INT TERM

usage()
{
	cat <<-EOF
	container script

	Input options:
	-b, --based            Based old image
	-o, --output           Generate new image
	-s, --script           Install script
	-a, --archive          Archive image
	-u, --upload           Upload image

	Examples:
		./image.sh create -b registry.access.redhat.com/ubi9 -o rhel9
	EOF
}
progname=${0##*/}

if [ $# -lt 2 ]; then
	usage >&2
	exit 1
fi

ACTION=$1; shift

_at=$(getopt -o b:o:s:auh \
	--long based: \
	--long output: \
	--long script: \
	--long archive \
	--long upload \
	--long help \
	-n "$progname" -- "$@")
eval set -- "$_at"

while true; do
	case "$1" in
		-b|--based)         BASE_IMAGE=$2; shift 2;;
		-o|--output)        NEW_IMAGE="$2"; shift 2;;
		-s|--script)        INSTALL_FILE=$2; shift 2;;
		-a|--archive)       ARCHIVE="yes"; shift 1;;
		-u|--upload)        UPLOAD="yes"; shift 1;;
		-h|--help)          usage >&2;exit 0;;
		--)                 shift;break;;
		*)                  echo "Error: Could not parse parameter $1!";exit 1;;
	esac
done

image_actions