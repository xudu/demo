#!/bin/bash
loopy_wait()
{
	local sleep_cmd=$1; shift
	local timeout_ms=$1; shift
	local command=("$@")

	local start_time
	start_time="$(date -u +%s%3N)"
	while true
	do
        eval "${command[*]}"
        if [ $? -ne 0 ]; then
            return 1
        fi
		local current_time
		current_time="$(date -u +%s%3N)"
		if ((current_time - start_time > timeout_ms)); then
			return 1
		fi
		$sleep_cmd
	done
}

run_with_timeout()
{
	local timeout_sec=$1
	shift
	loopy_wait "sleep 0.1" "$((timeout_sec * 1000))" "$@"
}

IFACE=ens1f0
total_vfs=$(cat /sys/class/net/"$IFACE"/device/sriov_totalvfs)
total_cpus=$(nproc)
vfs=(0 "$total_vfs")
cpus=("$total_cpus" $((num_cpus/2)))
K=0
J=0

sriov_vfmac_is_zero()
{
	pf_bus_info=$(ethtool -i "$1" | grep 'bus-info'| sed 's/bus-info: //')

	if ! ls /sys/bus/pci/devices/"$pf_bus_info"/virtfn* 1> /dev/null 2>&1; then
		return 0
	fi
	vf_bus_info=$(ls -lv /sys/bus/pci/devices/"$pf_bus_info"/virtfn* | awk '{print $NF}' | sed 's/..\///')
	for bus_info in $vf_bus_info; do
		vf_iface=$(ls /sys/bus/pci/devices/"$bus_info"/net 2>/dev/null)
		vfmac=$(cat /sys/class/net/"$vf_iface"/address)
        if [ "$vfmac" = "00:00:00:00:00:00" ]; then
            return 1
        fi
	done

	return 0
}

ethtool_combined()
{
    ethtool -L $IFACE combined "${cpus[J]}"
    ((J=(J+1)%2))

    if ! sriov_vfmac_is_zero $IFACE; then
        echo "Vf mac is zero"
        return 1
    fi
}

enable_sriov()
{
    echo "${vfs[K]}" > /sys/class/net/$IFACE/device/sriov_numvfs
    ((K=(K+1)%2))

    if ! sriov_vfmac_is_zero $IFACE; then
        echo "Vf mac is zero"
        return 1
    fi
}

run() {
    killall()
    {
        pkill -P $$
        exit 1
    }

    trap 'killall' CHLD

    run_with_timeout 100 "ethtool_combined" &
    run_with_timeout 100 "enable_sriov  >/dev/null" &
    wait
}

run
