#!/bin/python3
import os
import re
import sys
import time
import random
import signal
import traceback
import subprocess
from multiprocessing import Process

TEST_NIC = "ens6f0"
TEST_INTERVAL = 1000
TEST_THRESHOLD = 5.0


def handler(sig, frame):
    pgrp = os.getpgrp()
    os.killpg(pgrp, signal.SIGTERM)
    sys.exit(0)


def run(cmd):
    try:
        with subprocess.Popen(
            cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
        ) as proc:
            output, errors = proc.communicate(timeout=180)
            return proc.returncode, output
    except Exception as e:
        traceback.print_exc()


def run_exit(cmd):
    err, ret = run(cmd)
    if err != 0:
        print("execute %s, err %s" % (cmd, err))
        sys.exit(1)
    return ret


def save_perf_data(strace_file, perf_file):
    max_value = 0.0

    with open(strace_file, "r") as file:
        for line in file:
            match = re.search(r"MTU.*<(\d+\.\d+)>$", line)
            if match:
                value = float(match.group(1))
                max_value = max(max_value, value)

    if max_value > TEST_THRESHOLD:
        new_strace_file = f"strace_{max_value:.6f}.data"
        new_perf_file = f"perf_{max_value:.6f}.data"
        os.rename(strace_file, new_strace_file)
        os.rename(perf_file, new_perf_file)
    else:
        os.remove(strace_file)
        os.remove(perf_file)


def set_nic_port_mtu(nic_name):
    idx, mtu_val = 0, [1500, 9000]
    strace_file = "trace.data"
    perf_file = "perf.data"

    start_time = time.time()
    while time.time() - start_time < TEST_INTERVAL:
        try:
            time.sleep(random.uniform(0, 1))
            cmd = "perf record -a -g  --call-grap fp  -a -o {} -- sleep 10".format(
                perf_file
            )
            proc = subprocess.Popen(
                cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
            )
            run(
                "strace -t -T -e sendmsg ip link set {} mtu {}  2> {}".format(
                    nic_name, mtu_val[idx % 2], strace_file
                )
            )
            idx += 1
            outs, errs = proc.communicate(timeout=15)
            save_perf_data(strace_file, perf_file)
        except subprocess.TimeoutExpired as e:
            proc.kill()
        except Exception as e:
            traceback.print_exc()


def set_nic_port_updown(nic_name):
    idx, status, mtu_val = 0, ["up", "down"], [2000, 3000]
    start_time = time.time()
    while time.time() - start_time < TEST_INTERVAL:
        try:
            run(
                "nmcli connection modify {} 802-3-ethernet.mtu {}".format(
                    nic_name, mtu_val[idx % 2]
                )
            )
            run("nmcli connection {} {}".format(status[idx % 2], nic_name))
            idx += 1
            time.sleep(random.uniform(0, 1))
            run(
                "nmcli connection modify {} 802-3-ethernet.mtu {}".format(
                    nic_name, mtu_val[idx % 2]
                )
            )
            run("nmcli connection {} {}".format(status[idx % 2], nic_name))
        except:
            traceback.print_exc()


def setup(nic_name):
    run_exit("systemctl stop openvswitch")
    run_exit("ip link set {} up".format(nic_name))
    run("nmcli con delete {}".format(nic_name))
    run_exit(
        "nmcli con add type ethernet conn.interface {} con-name {}".format(
            nic_name, nic_name
        )
    )
    run_exit("nmcli con up {}".format(nic_name))

    time.sleep(10)

    run_exit("systemctl start openvswitch")
    run_exit("ovs-vsctl set Open_vSwitch . other_config={}")
    run_exit("ovs-vsctl set Open_Vswitch . other_config:dpdk-extra=' -n 4'")
    run_exit(
        "ovs-vsctl set Open_Vswitch . other_config:dpdk-lcore-mask='1000010000100001'"
    )
    run_exit(
        "ovs-vsctl set Open_Vswitch . other_config:pmd-cpu-mask='2000020000200002'"
    )
    run_exit("ovs-vsctl set Open_vSwitch . other_config:dpdk-init=true")

    run_exit(
        "ovs-vsctl --may-exist add-br ovsbr0 -- set bridge ovsbr0 datapath_type=netdev"
    )
    run_exit(
        "ovs-vsctl --may-exist add-port ovsbr0 {} -- set Interface {} type=dpdk options:dpdk-devargs=0000:98:00.0".format(
            nic_name, nic_name
        )
    )
    run_exit("ip link set ovsbr0 up")
    run_exit("nmcli con up ovsbr0")


# yum install -y perf strace dpdk NetworkManager-ovs
# yum install -y https://download.eng.bos.redhat.com/brewroot/vol/rhel-8/packages/openvswitch-selinux-extra-policy/1.0/30.el8fdp/noarch/openvswitch-selinux-extra-policy-1.0-30.el8fdp.noarch.rpm
# yum install -y https://download.eng.bos.redhat.com/brewroot/vol/rhel-8/packages/openvswitch2.15/2.15.0/9.el8fdp/x86_64/openvswitch2.15-2.15.0-9.el8fdp.x86_64.rpm
# grubby --args="default_hugepagesz=1G hugepagesz=1G hugepages=48 isolcpus=1-63 skew_tick=1 nohz=on nohz_full=1-63 rcu_nocbs=1-63 tuned.non_isolcpus=0" --update-kernel `grubby --default-kernel`

# cat /proc/cmdline
# BOOT_IMAGE=(hd0,msdos1)/vmlinuz-4.18.0-305.65.1.el8_4.x86_64 root=/dev/mapper/rhel_dell--per750--31-root ro ksdevice=bootif crashkernel=auto
# resume=/dev/mapper/rhel_dell--per750--31-swap rd.lvm.lv=rhel_dell-per750-31/root rd.lvm.lv=rhel_dell-per750-31/swap console=ttyS0,115200n81
# intel_iommu=on iommu=pt default_hugepagesz=1G hugepagesz=1G hugepages=48 isolcpus=1-63 skew_tick=1 nohz=on nohz_full=1-63 rcu_nocbs=1-63 tuned.non_isolcpus=0

if __name__ == "__main__":
    signal.signal(signal.SIGINT, handler)
    try:
        print("Setup environment")
        setup(TEST_NIC)

        pnum, pids = 2, []
        func = [set_nic_port_mtu, set_nic_port_updown]

        print("Start test")
        for i in range(pnum):
            p = Process(target=func[i], args=(TEST_NIC,))
            pids.append(p)

        for i in range(pnum):
            pids[i].start()

        for i in range(pnum):
            pids[i].join()

        print("Finish test")
    except KeyboardInterrupt:
        handler(signal.SIGINT, None)
