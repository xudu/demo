#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#define TABLE_SIZE 1024

typedef struct HashNode {
    int fd;
    struct sockaddr_storage addr;
    struct HashNode *next;
} HashNode;

typedef struct HashTable {
    HashNode *table[TABLE_SIZE];
} HashTable;

extern HashTable* create_hash_table();
extern int insert(HashTable *ht, int fd, struct sockaddr_storage *addr);

unsigned int hash(int fd) {
    return (unsigned int)(fd % TABLE_SIZE);
}

HashTable* create_hash_table() {
    HashTable *ht = (HashTable *)malloc(sizeof(HashTable));
    if (!ht) {
        perror("malloc");
        exit(EXIT_FAILURE);
    }
    memset(ht, 0, sizeof(HashTable));
    return ht;
}

int insert(HashTable *ht, int fd, struct sockaddr_storage *addr) {
    unsigned int index = hash(fd);
    HashNode *new_node = (HashNode *)malloc(sizeof(HashNode));
    if (!new_node) {
        perror("malloc");
        return 1;
    }
    new_node->fd = fd;
    memcpy(&new_node->addr, addr, sizeof(struct sockaddr_storage));
    new_node->next = ht->table[index];
    ht->table[index] = new_node;
    return 0;
}

HashNode* find(HashTable *ht, int fd) {
    unsigned int index = hash(fd);
    HashNode *node = ht->table[index];
    while (node) {
        if (node->fd == fd) {
            return node;
        }
        node = node->next;
    }
    return NULL;
}

void delete(HashTable *ht, int fd) {
    unsigned int index = hash(fd);
    HashNode *node = ht->table[index];
    HashNode *prev = NULL;
    while (node) {
        if (node->fd == fd) {
            if (prev) {
                prev->next = node->next;
            } else {
                ht->table[index] = node->next;  // 删除头节点
            }
            free(node);
            return;
        }
        prev = node;
        node = node->next;
    }
    printf("Socket fd %d not found in hash table.\n", fd);
}

void clear(HashTable *ht) {
    for (int i = 0; i < TABLE_SIZE; i++) {
        HashNode *node = ht->table[i];
        while (node) {
            HashNode *temp = node;
            node = node->next;
            free(temp);
        }
        ht->table[i] = NULL;
    }
}
void free_hash_table(HashTable *ht) {
    clear(ht);
    free(ht);
}

void print_address(struct sockaddr_storage *addr) {
    char ipstr[INET6_ADDRSTRLEN];
    if (addr->ss_family == AF_INET) {
        struct sockaddr_in *s = (struct sockaddr_in *)addr;
        inet_ntop(AF_INET, &(s->sin_addr), ipstr, sizeof(ipstr));
        printf("IPv4 Address: %s\n", ipstr);
    } else if (addr->ss_family == AF_INET6) {
        struct sockaddr_in6 *s = (struct sockaddr_in6 *)addr;
        inet_ntop(AF_INET6, &(s->sin6_addr), ipstr, sizeof(ipstr));
        printf("IPv6 Address: %s\n", ipstr);
    }
}
