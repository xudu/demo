#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/virtio_net.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/in.h>
#include <linux/if_tun.h>
#include <fcntl.h>

#define BUF_SIZE 2048
#define DATA_SIZE 8
#define TCP_HEADER_LEN 60

unsigned short checksum(void *b, int len) {
    unsigned short *buf = b;
    unsigned int sum = 0;
    unsigned short result;

    for (sum = 0; len > 1; len -= 2)
        sum += *buf++;
    if (len == 1)
        sum += *(unsigned char *)buf;
    sum = (sum >> 16) + (sum & 0xFFFF);
    sum += (sum >> 16);
    result = ~sum;
    return result;
}

int main(int argc, char **argv) {
    char packet[BUF_SIZE];
    struct virtio_net_hdr vnet_hdr;
    struct iphdr iph;
    struct tcphdr tcph;
    int tcph_option_len, len = 0;
    char *tcph_option = NULL;
    char payload[DATA_SIZE];
    int sockfd, fd;
    struct sockaddr_ll sa;
    struct ifreq ifr;
    int v = 1;

    if (argc != 2 || argv[1] == NULL || strncmp(argv[1], "tun", 3)) {
        fprintf(stderr, "missing tun interface!\n");
        return 1;
    }

    memset(&ifr, 0, sizeof(struct ifreq));
    strncpy(ifr.ifr_name, argv[1], IFNAMSIZ - 1);

    if ((fd = open("/dev/net/tun", O_RDWR)) < 0) {
        perror("open tun dev failed");
        goto out;
    }

    ifr.ifr_flags = (IFF_TUN | IFF_NO_PI);
    if (ioctl(fd, TUNSETIFF, (void *) &ifr) < 0 ) {
        perror("setting TUNSETIFF failed");
        goto cleanfd;
    }

    sockfd = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if (sockfd < 0) {
        perror("socket creation failed");
        goto cleanfd;
    }

    if (setsockopt(sockfd, SOL_PACKET, PACKET_VNET_HDR, &v, sizeof(v)) < 0) {
        perror("setsockopt(PACKET_VNET_HDR)\n");
        goto cleansock;
    }

    ifr.ifr_flags |= IFF_UP;
    if (ioctl(sockfd, SIOCSIFFLAGS, &ifr) < 0) {
        perror("setting interface UP");
        goto cleansock;
    }

    if (ioctl(sockfd, SIOCGIFINDEX, &ifr) < 0) {
        perror("ioctl failed");
        goto cleansock;
    }

    memset(&sa, 0, sizeof(struct sockaddr_ll));
    sa.sll_protocol = htons(ETH_P_ALL);
    sa.sll_ifindex = ifr.ifr_ifindex;

    memset(&vnet_hdr, 0, sizeof(struct virtio_net_hdr));
    vnet_hdr.flags = 0;
    vnet_hdr.gso_size = 8;
    vnet_hdr.gso_type = VIRTIO_NET_HDR_GSO_TCPV4;
    vnet_hdr.hdr_len = 80;
    vnet_hdr.csum_start = 0;
    vnet_hdr.csum_offset = 0;

    memset(&iph, 0, sizeof(struct iphdr));
    iph.version = 4;
    iph.ihl = 5;
    iph.tot_len = sizeof(struct iphdr) + 8;
    iph.id = htonl(54321);
    iph.frag_off = 0;
    iph.ttl = 255;
    iph.protocol = IPPROTO_TCP;
    iph.check = 0;
    iph.saddr = inet_addr("192.168.0.1");
    iph.daddr = inet_addr("192.168.0.2");
    iph.check = checksum(&iph, sizeof(struct iphdr));

    memset(&tcph, 0, sizeof(struct tcphdr));
    tcph.source = htons(1234);
    tcph.dest = htons(80);
    tcph.seq = htonl(0);
    tcph.ack_seq = 0;
    tcph.doff = TCP_HEADER_LEN / 4;
    tcph.syn = 1;
    tcph.window = htons(5840);
    tcph.check = 0;
    tcph.urg_ptr = 0;
    tcph_option_len = TCP_HEADER_LEN - sizeof(struct tcphdr);
    if (tcph_option_len > 0) {
        tcph_option = malloc(tcph_option_len);
        memset(tcph_option, 0x01, tcph_option_len);
    }

    memset(payload, 'x', DATA_SIZE);

    memcpy(packet, &vnet_hdr, sizeof(vnet_hdr));
    len += sizeof(vnet_hdr);
    memcpy(packet + len, &iph, sizeof(iph));
    len += sizeof(iph);
    memcpy(packet + len, &tcph, sizeof(tcph));
    len += sizeof(tcph);
    if (tcph_option) {
        memcpy(packet + len, &tcph_option, tcph_option_len);
        len += tcph_option_len;
    }
    /*memcpy(packet + len, payload, sizeof(payload));
    len += sizeof(payload);*/

    if (sendto(sockfd, packet, len, 0, (struct sockaddr*)&sa, sizeof(struct sockaddr_ll)) < 0) {
        perror("Send failed");
        goto cleansock;
    }

    printf("Packet sent successfully\n");

    close(sockfd);
    close(fd);
    return 0;

cleansock:
    close(sockfd);
cleanfd:
    close(fd);
out:
    return 1;
}
