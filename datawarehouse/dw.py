#!/usr/bin/python3
import os
import re
import sys
import time
import yaml
import argparse
import subprocess
import traceback
import requests

from lxml import etree
from proton import Message
from proton.utils import BlockingConnection
from proton import SSLDomain
from proton import SSL as pSSL
import OpenSSL.SSL
from OpenSSL import crypto

# environment
BASE_PATH = os.getcwd()
DATA_PATH = os.path.join(BASE_PATH, "data")
ETC_PATH = os.path.join(BASE_PATH, "etc")
CONF_PATH = os.path.join(BASE_PATH, "config")
OUPUT_NAME = "output.json"
OUPUT_FILE = os.path.join(DATA_PATH, OUPUT_NAME)

MAX_MSG_SIZE = 8 * 1024 * 1024
IMAGE = "registry.gitlab.com/redhat/centos-stream/tests/kernel/kernel-qe-tools/kernel-qe-tools:production"
# Service account cert and key required to send the message
CERT = os.path.join(ETC_PATH, "kernel-qe.crt")
KEY = os.path.join(ETC_PATH, "kernel-qe.key")
CACERT = os.path.join(ETC_PATH, "2022-IT-Root-CA.pem")
ROOT_CACERT = os.path.join(ETC_PATH, "2022-IT-Root-CA.pem")
URLS_AND_FILENAMES = [
    (
        "https://gitlab.cee.redhat.com/kernel-qe/kernel-ci/-/raw/master/shared/umb/kernel-qe.crt?ref_type=heads&inline=false",
        CERT,
    ),
    (
        "https://gitlab.cee.redhat.com/kernel-qe/kernel-ci/-/raw/master/shared/umb/kernel-qe.key?ref_type=heads&inline=false",
        KEY,
    ),
    (
        "https://gitlab.cee.redhat.com/kernel-qe/kernel-ci/-/raw/master/shared/umb/2022-IT-Root-CA.pem?ref_type=heads&inline=false",
        CACERT,
    ),
    (
        "https://gitlab.cee.redhat.com/kernel-qe/kernel-ci/-/raw/master/shared/umb/RH-IT-Root-CA.crt?ref_type=heads&inline=false",
        ROOT_CACERT,
    ),
]
# UMB message topic to send DataWarehouse results
TOPIC = "topic://VirtualTopic.eng.cki.results"


# functions
def wrap_retry(max_times=3, seconds=1):
    def decorator(func):
        def wrapper(self, *args, **kwargs):
            for cnt in range(max_times):
                try:
                    return func(self, *args, **kwargs)
                except Exception as e:
                    if cnt < (max_times - 1):
                        traceback.print_exc()
                        time.sleep(seconds)
                    else:
                        traceback.print_exc()
                        return 0

        return wrapper

    return decorator


def command(cmd):
    try:
        with subprocess.Popen(
            cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
        ) as proc:
            rc = proc.returncode
            output = proc.stdout.read()
            return rc, output
    except Exception as e:
        traceback.print_exc()


def open_umb():
    try:
        with open(CERT, encoding="utf-8") as f:
            contents = f.read()
            cert = crypto.load_certificate(crypto.FILETYPE_PEM, contents)
        with open(KEY, encoding="utf-8") as f:
            contents = f.read()
            key = crypto.load_privatekey(crypto.FILETYPE_PEM, contents)
    except IOError:
        print("Failed to load DataWarehouse cert - likely bad kernel-ci path")

    ctx = OpenSSL.SSL.Context(OpenSSL.SSL.TLSv1_METHOD)
    ctx.use_privatekey(key)
    ctx.use_certificate(cert)
    # Will raise on any key mismatch errors:
    ctx.check_privatekey()

    # Verify SSL is available and initialize SSLDomain
    # Details: https://packetstormsecurity.com/files/136403/Apache-Qpid-Proton-0.12.0-SSL-Failure.html
    if not pSSL.present():
        print(
            "Proton SSL is not present on this machine - cannot upload to DataWarehouse"
        )
        print(
            "This host is most likely missing the dependencies required by dw_publish.py"
        )
        sys.exit(1)
    ssl = SSLDomain(SSLDomain.MODE_CLIENT)
    ssl.set_credentials(CERT, KEY, None)
    ssl.set_trusted_ca_db(CACERT)
    ssl.set_peer_authentication(SSLDomain.VERIFY_PEER)

    conn = BlockingConnection(
        "amqps://umb.api.redhat.com:5671", ssl_domain=ssl, timeout=180
    )
    sender = conn.create_sender(TOPIC)
    return conn, sender


def close_umb(conn):
    conn.close()


@wrap_retry(3, 100)
def publish_umb(checkout, outfile):
    conn, sender = open_umb()

    msg_prop = {"type": "brew-build", "id": checkout}
    with open(outfile, encoding="utf-8") as f:
        msg_body = f.read()

    if sender.send(Message(properties=msg_prop, body=msg_body), timeout=60):
        print(f"publish {checkout} to datawarehouse successfully")
    else:
        print(f"publish {checkout} to datawarehouse failed")

    close_umb(conn)


def parse_config(file):
    conf = {}
    with open(file=file, mode="r", encoding="utf-8") as f:
        content = yaml.load(stream=f.read(), Loader=yaml.FullLoader)

    conf["testcase"] = {}
    if content["testcase"]:
        for test in content["testcase"]:
            with open(file=test, mode="r", encoding="utf-8") as f:
                for line in f.readlines():
                    m = re.match(r"(-\s)?(/kernel/networking/[^:\s]+)", line)
                    if not m:
                        continue
                    case = m.group(2)
                    m = re.search(r"testside:\s*(\w+)", line)
                    if m and m.group(1).strip(",") == "client":
                        side = 2
                    else:
                        side = 1
                    conf["testcase"].setdefault(case, side)
    return conf


@wrap_retry(3, 10)
def bkrjob_2_kcidb(job, nvr, checkout, driver, cases):
    test_side = 1

    command(f"bkr job-results {job} > {DATA_PATH}/job.orig.xml")
    tree = etree.parse(f"{DATA_PATH}/job.orig.xml")
    tasks = set(tree.xpath("//recipe[1]/task/@name"))
    if cases:
        exist = list(set(tasks) & set(cases.keys()))
        if not exist:
            print(f"{job} don't need publish to datawarehouse")
            return 0
        else:
            test_side = 2 if cases[exist[0]] == 2 else 1

    if driver:
        test_driver = driver
    else:
        test_driver = tree.xpath(
            f"//recipe[{test_side}]//param[@name='NIC_DRIVER']/@value"
        )[0]

    parts = nvr.split("-")
    test_nvr = parts[0] + f"-{test_driver}-" + "-".join(parts[1:])

    formatted = etree.tostring(tree, pretty_print=True, encoding="unicode")
    formatted = re.sub(r"http.*#", "", formatted)
    formatted = re.sub(r"git.*#", "", formatted)
    with open(f"{DATA_PATH}/job.xml", "w") as f:
        f.write(formatted)

    cmd = (
        f"podman run -v {DATA_PATH}:/output:Z -w /output --rm {IMAGE} bkr2kcidb "
        f"create --nvr {test_nvr} --contact network-qe@redhat.com -i ./job.xml -c {checkout} -o ./{OUPUT_NAME}"
    )
    rc, _ = command(cmd)
    if not rc:
        print(f"{job} {test_nvr} transform ckidb successfully")
        return 1
    else:
        print(f"{job} {test_nvr} transform ckidb failed")
        return 0


def download_certs_files(urls_and_filenames):
    for url, filename in urls_and_filenames:
        requests.packages.urllib3.disable_warnings()
        response = requests.get(url, verify=False, stream=True)
        response.raise_for_status()

        with open(filename, "wb") as file:
            for chunk in response.iter_content(chunk_size=8192):
                file.write(chunk)
        print(url, filename)


def parse_jobs(job=None, wb=None, owner=None):
    if job:
        return job

    if args.wb and args.owner:
        cmd = "" if not args.wb else f" -w {args.wb}"
        cmd += "" if not args.owner else f" -o {args.owner}"
        cmd = "bkr job-list" + cmd + " --format list"
        _, o = command(cmd)
        jobs = bytes.decode(o, encoding="utf8").splitlines()
        return jobs


def cleanup():
    os.system(f"rm -rf {DATA_PATH}/*")


if __name__ == "__main__":
    for path in [DATA_PATH, ETC_PATH]:
        if not os.path.exists(path):
            os.makedirs(path)

    download_certs_files(URLS_AND_FILENAMES)

    parser = argparse.ArgumentParser()
    parser.add_argument("--checkout", dest="checkout", required=True, type=str)
    parser.add_argument("--nvr", dest="nvr", required=True, type=str)
    parser.add_argument("--job", dest="job", required=False, nargs="+")
    parser.add_argument("--wb", dest="wb", required=False, type=str)
    parser.add_argument("--owner", dest="owner", required=False, type=str)
    parser.add_argument("--driver", dest="driver", required=False, type=str)
    parser.add_argument("--update", action="store_true", required=False)
    args = parser.parse_args()

    nvr = args.nvr
    checkout = args.checkout
    driver = args.driver

    rc, _ = command(f"podman inspect -f '{{.RepoTags}}' {IMAGE}")
    if rc or args.update:
        command(f"podman image pull {IMAGE}")

    conf = parse_config(os.path.join(CONF_PATH, "conf.yml"))
    if not conf:
        print("open_umb failed")
        sys.exit(1)

    jobs = parse_jobs(args.job, args.wb, args.owner)
    if not jobs:
        print("Unspecified job id or whiteboard")
        sys.exit(1)

    try:
        for job in jobs:
            bkrjob_2_kcidb(job, nvr, checkout, driver, conf["testcase"])
            if os.path.exists(OUPUT_FILE):
                if (
                    os.path.getsize(OUPUT_FILE) > MAX_MSG_SIZE
                    or job == jobs[-1]
                ):
                    publish_umb(checkout, OUPUT_FILE)
                    cleanup()
    except Exception as e:
        traceback.print_exc()
    finally:
        cleanup()
