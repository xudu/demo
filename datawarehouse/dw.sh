#!/bin/bash

#-- variables ----
TASKID="999999"
CONTACT="network-qe@redhat.com"
KCI_DIR="/home/xudu/kernel-ci"
IMAGE="registry.gitlab.com/redhat/centos-stream/tests/kernel/kernel-qe-tools/kernel-qe-tools:production"
OPTS=()
MAX_RETRIES=2

#-- functions ----
publish_datawarehouse()
{
    local nvr=$1
    local checkout=$2
    local exitcode=0

    xmllint --format dw/job.orig.xml | sed 's/http.*#//;s/git.*#//' > dw/job.xml

    declare bkr_to_kci="podman run -v $PWD/dw:/output:Z -w /output --rm $IMAGE bkr2kcidb"
    $bkr_to_kci create --nvr "$nvr" --contact "$CONTACT" -i job.xml -c "$checkout" -o output.json
    [ $? -ne 0 ] && (( exitcode++ ))

    # in the future, bkr2kcidb will add 'push2umb' functionality as well -
    # see https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-qe-tools/-/merge_requests/36
    "$KCI_DIR"/shared/utils/python/dw_publish.py -p "$KCI_DIR" -b $TASKID -m dw/output.json
    [ $? -ne 0 ] && (( exitcode++ ))
    return $exitcode
}

Usage()
{
	cat <<- END
    datawarehous tools

    Input options:
        --checkoutid        checkout id
        --nvr               name-version-resion
        --kci               kci directory
        --job               job id
        --wb                white board
        --owner             job owner
        --driver            driver
        --side              test side

    Examples:
        sh dw.sh --checkoutid RHEL9.4_RC --nvr kernel-5.14.0-427.11.1.el9_4 --wb RHEL9.4_RC --owner liali --side server
        sh dw.sh --checkoutid 13223r4 --nvr kernel-5.14.0-284.57.1.el9_2 --job "J:9065232 J:9065233" --driver i40e
	END
}

#-- parameters ----
progname=${0##*/}
_at=$(getopt -o uh \
    --long checkoutid: \
    --long nvr: \
    --long kci: \
    --long job: \
    --long wb: \
    --long owner: \
    --long driver: \
    --long side: \
    --long help \
    -n "$progname" -- "$@")
eval set -- "$_at"

while true; do
	case "$1" in
        --checkoutid)   CHECKOUTID="$2"; shift 2;;
        --nvr)          NVR="$2"; shift 2;;
        --kci)          KCI_DIR="$2"; shift 2;;
        --job)          JOBIDS="$2"; shift 2;;
        --wb)           OPTS+=(-w "$2"); shift 2;;
        --owner)        OPTS+=(-o "$2"); shift 2;;
        --driver)       DRIVER="$2"; shift 2;;
        --side)         SIDE="$2"; shift 2;;
        -u)             UPDATE=true; shift 1;;
        -h|--help)      Usage; shift 1; exit 0;;
        --) shift; break;;
	esac
done

if ! rpm -qa | grep podman > /dev/null || \
   ! rpm -qa | grep python3-qpid-proton > /dev/null || \
   ! rpm -qa | grep qpid-proton-c > /dev/null ; then
     echo "Need install package, 'dnf install -y podman python3-qpid-proton qpid-proton-c'"
     exit
fi

if [ ! -d "$KCI_DIR" ] ; then
     echo "Need clone kernel-ci, 'git clone git@gitlab.cee.redhat.com:kernel-qe/kernel-ci.git'"
     exit
fi

[ -d dw ] && rm -rf dw
mkdir dw

if [[ $UPDATE == true ]] || ! podman inspect -f '{{.RepoTags}}' $IMAGE > /dev/null; then
    podman image pull $IMAGE
fi

if [ -z "$JOBIDS" ]; then
    JOBIDS=$(bkr job-list "${OPTS[@]}" --format list)
fi

for JOBID in $JOBIDS; do
    bkr job-results "$JOBID" > dw/job.orig.xml

    TEST_DRIVER=$DRIVER
    if [ -z "$TEST_DRIVER" ]; then
        case $SIDE in 
            server)  
                TEST_SIDE=1;;
            client)  
                TEST_SIDE=2;;
            *)        
                TEST_SIDE=1;; 
        esac
        TEST_DRIVER=$(xmllint --xpath "string(//recipe[$TEST_SIDE]//param[@name='NIC_DRIVER']/@value)" dw/job.orig.xml)
    fi
    if [ -z TEST_DRIVER ] ; then
        echo "Error: skip $JOBID because job driver is NULL!!!"
        continue
    fi
    PREFIX=${NVR%%-*}
    SUFFIX=${NVR#*-}
    NEW_NVR="${PREFIX}-$TEST_DRIVER-${SUFFIX}"

    xmllint --format dw/job.orig.xml | sed 's/http.*#//;s/git.*#//' > dw/job.xml
    retries=0
    while ! publish_datawarehouse "$NEW_NVR" "$CHECKOUTID" ; do
        sleep 300
        (( retries++ ))
        if [ $retries -eq $MAX_RETRIES ]; then
            break
        fi
    done

    [ $retries -eq $MAX_RETRIES ] && {
        echo "Error: skip $JOBID because publish datawarehouse failed!!!"
        continue 
    }
done

rm -rf dw
echo "DataWarehouse link: https://datawarehouse.cki-project.org/kcidb/checkouts/bkr2kcidb:${CHECKOUTID}"
