#!/bin/bash

#############################################################################
# Environment
#############################################################################
RHEL=$(rpm -E %rhel)
SYS_ARCH=$(uname -m)
CPUS=4
GDB="no"
VIOMMU="no"
IMAGE_PATH=/var/lib/libvirt/images
IMAGE_CONF_PATH=/etc/libvirt
ACTION=""

USER="root"
PASSWORD="redhat"

#############################################################################
# Functions
#############################################################################
cleanup()
{
	rm -rf /var/www/html/inst.ks
}

sig_handler()
{
	cleanup
	exit 0
}

title()
{
	printf "\n\n\033[32m%s\033[0m\n\n" "$1"
}

function vm_cmd()
{
	local host=$1
    local command="$2"

    expect <<-EOF
        set timeout 20
        spawn ssh $USER@$host
        expect {
            "*yes/no*" {
                send "yes\r"
                exp_continue
            }
            "*password:*" {
                send "$PASSWORD\r"
            }
        }
        expect "*$USER@*\$ "
        send "$command\r"
        expect "*$USER@*\$ "
        send "exit\r"
        expect eof
	EOF
}

install_packages()
{
	rpm -qa httpd && dnf install httpd -y
	rpm -qa libvirt && dnf install libvirt -y
	rpm -qa virt-install && dnf install virt-install -y
	rpm -qa qemu-kvm && dnf install qemu-kvm -y
	rpm -qa expect && dnf install expect -y
}

enable_service()
{
	if (("$RHEL" <=6)); then
		service libvirtd restart
		service virtlogd restart
	elif (("$RHEL" < 9)); then
		systemctl start libvirtd
		systemctl start virtlogd
	else
		rm -f /usr/share/qemu/firmware/50-edk2-ovmf-amdsev.json
		for drv in qemu interface network nodedev nwfilter secret storage proxy
		do
			systemctl unmask virt${drv}d.service
			systemctl unmask virt${drv}d{,-ro,-admin}.socket
			systemctl enable virt${drv}d.service
			systemctl enable virt${drv}d{,-ro,-admin}.socket
			systemctl start virt${drv}d.service
			systemctl status virt${drv}d.service --no-pager -l
		done
		systemctl unmask libvirtd.service
		systemctl enable libvirtd.service
		systemctl unmask libvirtd{,-ro,-admin}.socket
		systemctl enable libvirtd{,-ro,-admin}.socket
	fi

	# SELinux
	ENFORCE=$(getenforce)
	setenforce permissive
}

disable_service()
{
	if (("$RHEL" <=6)); then
		service libvirtd stop
		service virtlogd stop
	elif (("$RHEL" < 9)); then
		systemctl stop libvirtd
		systemctl stop virtlogd
	else
		for drv in qemu interface network nodedev nwfilter secret storage proxy
		do
			systemctl stop virt${drv}d{,-ro,-admin}.socket
			systemctl stop virt${drv}d.service
		done
		systemctl stop libvirtd.service
		systemctl stop libvirtd{,-ro,-admin}.socket
	fi

	systemctl stop httpd

	setenforce "$ENFORCE"
}

vm_check()
{
	case "$ACTION" in
		create)
			if [ -z "$GUEST" ]; then
				echo "ERROR: missing guest name!"
				exit 1
			fi

			if [ -z "$DISTRO" ]; then
				echo "ERROR: distro missing!"
				exit 1
			elif ! curl --head --silent --fail "$DISTRO" > /dev/null; then
				echo "ERROR: distro unrechable!"
				exit 1
			fi

			if [ -z "$KS_URL" ]; then
				echo "ERROR: kickstart file missing!"
				exit 1
			elif (echo "$KS_URL" | grep -q http && ! curl --head --silent --fail "$KS_URL" > /dev/null) && [ ! -f "$KS_URL" ] ; then
				echo "ERROR: kickstart file unrechable!"
				exit 1
			fi
			;;
		*)
			;;
	esac
}

create_vm()
{
	title "Want to create Geust $GUEST"
	printf "    ARCH %s\n" "$SYS_ARCH"
	printf "    CORE %s\n" "$CPUS"
	printf "    support GDB %s\n" "$GDB"
	printf "    support VIOMMU %s\n" "$VIOMMU"

	title "Creating guest image..."
	if virsh list --all | awk '{print $2}' | grep -q "$GUEST" ; then
		virsh destroy "$GUEST"
		virsh undefine "$GUEST"
	fi

	GUEST_IMAGE="$GUEST".qcow2
	[ -f $IMAGE_PATH/"$GUEST_IMAGE" ] && rm -rf "${IMAGE_PATH:?}"/"$GUEST_IMAGE"
	qemu-img create -f qcow2 "$IMAGE_PATH"/"$GUEST_IMAGE" 100G

	virtbr=$(ip addr show virbr0 |  awk '/inet / {print $2}' | cut -d '/' -f 1)
	EXTRA="inst.ks=http://$virtbr/inst.ks console=ttyS0,115200 DISTRO=$DISTRO"

	if [[ $KS_URL =~ ^https?:// ]]; then
		curl -Lo /var/www/html/inst.ks "$KS_URL"
	else
		cp "$KS_URL" /var/www/html/inst.ks
	fi
	systemctl start httpd

	sed -i "s|BaseOSRepo|$DISTRO|g" /var/www/html/inst.ks
	sed -i "s|AppStreamRepo|${DISTRO//BaseOS/AppStream}|g" /var/www/html/inst.ks

	title "Installing virtual machine..."
	if ! virt-install --name="$GUEST"\
			--virt-type=kvm\
			--disk path=$IMAGE_PATH/"$GUEST_IMAGE",format=qcow2,,size=3,bus=virtio\
			--vcpus="$CPUS"\
			--ram=4096\
			--network bridge=virbr0\
			--graphics none\
			--extra-args="$EXTRA"\
			--location="$DISTRO"\
			--noreboot\
			--serial pty\
			--serial file,path=/tmp/"$GUEST".console &> /tmp/vminstaller.log
	then
		printf "Install virtual machine %s failed, pelease review vminstaller.log" "$GUEST"
		return 1
	fi

	if [[ $GDB = "yes" ]]; then
		#sed -i "s|.*domain type=.*|<domain type=\'kvm\' xmlns:qemu=\'http://libvirt.org/schemas/domain/qemu/1.0\'>|" \
		#	"$IMAGE_CONF_PATH"/qemu/"$GUEST".xml
		#sed -i "/<\/domain>/i\  <qemu:commandline>\n      <qemu:arg value=\'-s\'/>\n  <\/qemu:commandline>" \
		#	"$IMAGE_CONF_PATH"/qemu/"$GUEST".xml
		#sed -i "/<\/domain>/i\  <qemu:commandline>\n      <qemu:arg value=\'-S\'/>\n  <\/qemu:commandline>" \
		#	"$IMAGE_CONF_PATH"/qemu/"$GUEST".xml
		#virsh define "$IMAGE_CONF_PATH"/qemu/"$GUEST".xml
		virt-xml "$GUEST" --edit --qemu-commandline "-s -S"
		virsh start "$GUEST"
		sleep 30

		VM_MAC=$(virsh domiflist g1 | grep bridge | awk '{print $5}')
		VM_IP=$(virsh net-dhcp-leases default | grep "$VM_MAC" | awk '{print $5}' | awk -F "/" '{print $1}')
		vm_cmd "$VM_IP" "grubby --args='kgdboc=ttyS0,115200 nokaslr' --update-kernel=ALL"
		vm_cmd "$VM_IP" "reboot"
	fi
}

vm_actions()
{
	case "$ACTION" in
		create)
			install_packages > /dev/null

			enable_service > /dev/null

			create_vm
			;;
		*)
			echo "Error: could not parse actions"
			exit 1
			;;
	esac
}

usage()
{
	cat <<- EOF
	Virtual machine scripts

	Input options:
	-d, --distro           VM distro
	-k, --ks               VM Kickstart file
	-c, --cpu              VM CPUs
	-g, --guest            VM name
	-D, --debug            VM support debug
	-V, --viommu           VM support iommu

	Examples:
		# create
		./vm.sh create -c 4 -d http://download.eng.pek2.redhat.com/rhel-9/nightly/RHEL-9/latest-RHEL-9.5.0/compose/BaseOS/x86_64/os/ -k ./ks.cfg --guest g1 --gdb
	EOF
}

# program
trap "sig_handler" EXIT INT TERM

progname=${0##*/}

if [ $# -lt 1 ]; then
	echo "Error: too few arguments" >&2
	exit 1
fi

ACTION=$1
shift

_at=$(getopt -o d:k:c:h \
	--long distro: \
	--long ks: \
	--long cpu: \
	--long guest: \
	--long gdb \
	--long viommu \
	--long help \
	-n "$progname" -- "$@")
eval set -- "$_at"

while true; do
	case "$1" in
		-d|--distro)  	DISTRO="$2"; shift 2;;
		-k|--ks)        KS_URL=$2; shift 2;;
		-c|--cpu)       CPUS="$2"; shift 2;;
		-h|--help)      usage; shift; exit 0;;
		--guest)     	GUEST="$2"; shift 2;;
		--viommu)       VIOMMU="yes"; shift;;
		--gdb)          GDB="yes"; shift;;
		--)             shift;break;;
		*)              echo "Error: could not parse arguments" >&2;exit 1;;
	esac
done

vm_check

vm_actions