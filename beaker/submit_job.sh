#!/bin/bash

# enviroment
SCRIPT_DIR=$(pwd -P)
BASE_PATH=$(dirname "$(dirname "$SCRIPT_DIR")")
SH_TASKFILE="sh-tasks.list"
MH_TASKFILE="mh-tasks.list"
LOCKFILE="acquire.lock"
SHAREFILE="share.txt"

# parameters
OPTS=()
SOPTS=()
COPTS=()
PARAMS=(
"--arch=x86_64"
"--retention-tag=active+1"
"--product=cpe:/o:redhat:enterprise_linux"
"--ks-meta=redhat_ca_cert no_networks"
"--param=SET_DMESG_CHECK_KEY=yes"
"--param=NAY=yes"
"--k-opts=selinux=0 enforcing=0"
"--k-opts-post=selinux=0 enforcing=0"
"--nrestraint"
"--autopath"
#"--fetch-url=/distribution@http://netqe-bj.usersys.redhat.com/share/distribution-master.tar.gz"
)
COMMAND=""

# options
GOV=no
TOPO=
ALL=no
FORCE=no
CURRENT=
DEF_MATCH="driver-prototype"
LOC=(driver-Prototype--bj1 driver-Prototype--bj2 driver-Prototype)

# functions
cleanup()
{
	rm -f "$LOCKFILE" "$SHAREFILE" "$SH_TASKFILE" "$MH_TASKFILE"
}

setup()
{
	cleanup
	touch "$LOCKFILE" "$SHAREFILE"
}

search_available_system()
{
	local opts=("${@}")
	local raw_info=$(./parse_netqe_nic_info.sh --raw "${opts[@]}")
	awk -v t="${opts[*]}" '{
		split(t, opts, " ")
		f1 = $3
		for (i = 1; i <= length(opts); i++) {
			if (opts[i] ~ "--driver") {
				f2 = $4
			}
			if (opts[i] == "--model") {
				f3 = $6
			}
			if (opts[i] == "--speed") {
				f4 = $7
			}
		}
		f5=$12
		print f1"|"f2"|"f3"|"f4"|"f5
	}' <<< "$raw_info" | sort -u | shuf
}

search_idle_system()
{
	dbg=$(set +o | grep xtrace)
	set +x
	local systems=$1
	get_idle_system()
	{
		local host=$(echo "$1" | awk -F '|' '{print $1}')
		local bkr_status=$(bkr system-status "$host" --format=json)
		local loan=$(jq -r ".current_loan // empty" <<< "$bkr_status")
		local loan_user=$(jq -r ".current_loan.user.email_address // empty" <<< "$bkr_status")
		local recipient_user=$(jq -r ".current_loan.recipient_user.email_address // empty" <<< "$bkr_status")
		local reserve_user=$(jq -r ".current_reservation.user.email_address // empty" <<< "$bkr_status")

		if [[ -z $loan || $loan_user =~ $2 || $recipient_user =~ $2 ]] && [[ -z $reserve_user ]]; then
			flock -x -w 360 "$LOCKFILE" -c "echo \"$1\" >> $SHAREFILE"
		fi
	}

	local email=$(jq -r ".email_address // empty" <<< $(bkr whoami))
	while IFS= read -r system; do
		get_idle_system "$system" "$email" &
	done <<< "$systems"

	wait
	cat $SHAREFILE
	[[ "$dbg" == "xtrace        on" ]] && set -x
}

select_test_system()
{
	local a1=() a2=()

	if [[ -z $1 || -z $2 ]] ; then
		return
	fi

	readarray -t a1 <<< "$1"
	readarray -t a2 <<< "$2"
	for l1 in "${a1[@]}"; do
		t1=$(echo "$l1" | awk -F '|' '{print $1}')
		for l2 in "${a2[@]}"; do
			t2=$(echo "$l2" | awk -F '|' '{print $1}')
			if [[ "$t1" != "$t2" ]]; then
				echo "$l1" "$l2"
				return
			fi
		done
	done
}

submit_singlehost_job()
{
	local avails="" idles="" systems="" params=("${PARAMS[@]}")

	avails=$(search_available_system "${SOPTS[@]}")
	if [ -z "$avails" ] ; then
		echo ":::ERROR::: no available system!"
		exit 1
	fi

	if [ "$ALL" = "yes" ] ; then
		systems=$avails
	else
		idles=$(search_idle_system "$avails")
		if [ -z "$idles" ] && [ "$FORCE" = "no" ] ; then
			echo ":::WARNING::: no idle system!"
			exit 1
		elif [ -z "$idles" ] ; then
			systems=$(echo "$avails" | head -n 1)
		else
			systems=$(echo "$idles" | head -n 1)
		fi
	fi

	local machine="" systype="" driver="" model="" speed=""

	while IFS= read -r system; do
		machine=$(echo "$system" | awk -F '|' '{print $1}')
		systype=$(echo "$system" | awk -F '|' '{print $5}' | awk -F '-' '{print $2}')

		driver=$(echo "$system" | awk -F '|' '{print $2}')
		[ -n "$driver" ] && params+=("--param=NIC_DRIVER=$driver")

		model=$(echo "$system" | awk -F '|' '{print $3}')
		[ -n "$model" ] && params+=("--param=NIC_MODEL=$model")

		speed=$(echo "$system" | awk -F '|' '{print $4}' | sed 's/[^0-9]//g')
		[ -n "$speed" ] && params+=("--param=NIC_SPEED=$speed")

		runtest "$DISTRO" --machine="$machine" --systype="$systype" "${params[@]}" "$@" < "$SH_TASKFILE"
	done <<< "$systems"
}

submit_multihost_job()
{
	local s_avails="" c_avails="" s_idles="" c_idles="" s_system="" c_system=""

	if [ $ALL = "yes" ] ; then
		echo ":::ERROR::: multihost topo need idle system!"
		exit 1
	fi

	s_avails=$(search_available_system "${SOPTS[@]}")
	c_avails=$(search_available_system "${COPTS[@]}")
	if [ -z "$s_avails" ] || [ -z "$c_avails" ] ; then
		echo ":::ERROR::: no available system!!!"
		exit 1
	fi

	s_idles=$(search_idle_system "$s_avails")
	c_idles=$(search_idle_system "$c_avails")

	local _prio=0 prio=0 s="" c="" t=""
	local temp="" s_temp="" c_temp="" s_cand="" c_cand=""

	for loc in "${LOC[@]}" ; do
		prio=3
		s=$(echo "$s_idles" | grep -E "$loc\$")
		c=$(echo "$c_idles" | grep -E "$loc\$")

		t=$(select_test_system "$s" "$c")
		s_system=$(echo "$t" | awk '{print $1}')
		c_system=$(echo "$t" | awk '{print $2}')

		if [[ -n "$s_system" && -n "$c_system" ]]; then
			break
		fi

		if [[ -z $s ]] ; then
			(( prio-= 1))
			s=$(echo "$s_avails" | grep -E "$loc\$")
		fi
		if [[ -z $c ]] ; then
			(( prio-= 1))
			c=$(echo "$c_avails" | grep -E "$loc\$")
		fi

		temp=$(select_test_system "$s" "$c")
		s_temp=$(echo "$temp" | awk '{print $1}')
		c_temp=$(echo "$temp" | awk '{print $2}')
		if [[ -n $s_temp && -n $c_temp ]] && [ $prio -gt $_prio ]; then
			s_cand=$s_temp
			c_cand=$c_temp
			_prio=$prio
		fi
	done

	if [[ -z "$s_system" || -z "$c_system" ]] ; then
		if [ "$FORCE" = "no" ]; then
			echo ":::WARNING::: no idle system!!!"
			exit 1
		fi

		if [[ -z $s_cand || -z $c_cand ]] ; then
			echo ":::ERROR::: no available system!!!"
			exit 1
		fi
		s_system=$s_cand
		c_system=$c_cand
	fi

	local params=("${PARAMS[@]}")
	local s_machine="" s_systype="" s_driver="" s_model="" s_speed=""
	local c_machine="" c_systype="" c_driver="" c_model="" c_speed=""

	s_machine=$(echo "$s_system" | awk -F '|' '{print $1}')
	s_systype=$(echo "$s_system" | awk -F '|' '{print $5}' | awk -F '-' '{print $2}')

	c_machine=$(echo "$c_system" | awk -F '|' '{print $1}')
	c_systype=$(echo "$c_system" | awk -F '|' '{print $5}' | awk -F '-' '{print $2}')

	s_driver=$(echo "$s_system" | awk -F '|' '{print $2}')
	c_driver=$(echo "$c_system" | awk -F '|' '{print $2}')
	if [ -n "$s_driver" ] && [ -n "$c_driver" ]; then
		params+=("--param=mh-NIC_DRIVER=$s_driver,$c_driver")
	fi

	s_model=$(echo "$s_system" | awk -F '|' '{print $3}')
	c_model=$(echo "$c_system" | awk -F '|' '{print $3}')
	if [ -n "$s_model" ] && [ -n "$c_model" ]; then
		params+=("--param=mh-NIC_MODEL=$s_model,$c_model")
	fi

	s_speed=$(echo "$s_system" | awk -F '|' '{print $4}' | sed 's/[^0-9]//g')
	c_speed=$(echo "$c_system" | awk -F '|' '{print $4}' | sed 's/[^0-9]//g')
	if [ -n "$s_speed" ] && [ -n "$c_speed" ]; then
		params+=("--param=mh-NIC_SPEED=$s_speed,$c_speed")
	fi

	runtest "$DISTRO" --machine="$s_machine,$c_machine" --systype="$s_systype,$c_systype" "${params[@]}" "$@"  < "$MH_TASKFILE"
}

Usage()
{
	cat <<- END
	Tools for submitting job

	Input options:
	-D, --distro        Distro kernel
	-B, --brew          Brew kernel
	-M, --match         Match system information
	-U, --unmatch       Unmatch system information
	-d, --driver        Match nic driver information
	-m, --model         Match nic model information
	-s, --speed         Match nic speed information
	-n, --num           Match nic number
	-T, --test          Test case
	-C, --cmd           Command and reboot
	-F, --file          Test file
	-f                  Force submit no idle system
	-a                  All match system or one system
	-c                  Current test branch
	--wb                Job white board
	--url               Fetch url
	--gov               GOV test

	Examples:
	# optical module eeprom extend info
	./submit_job.sh -D RHEL-8.10.0-20231121.1 --wb eeprom \\
	--test nic/functions/ethtool/sanity --unmatch wsfd-netdev -a -f \\
	-- --param=RUN_FUNC=eeprom_extended_info

	# nic consistent via leapp upgrade
	./submit_job.sh -D RHEL-8.10.0-updates-20240623.1 --wb RHEL8.10-RHEL9.5 \\
	-T nic/functions/nic_driver_info/,nic/functions/update_via_leapp,nic/functions/nic_driver_info -a -f \\
	-- --param=ORIGIN_VERSION=8.10 --param=TARGET_VERSION=9.5
	END
}

setup
trap "cleanup" EXIT INT TERM

progname=${0##*/}

_at=$(getopt -o D:B:M:U:d:m:s:n:T:C:F:acfh \
	--long distro: \
	--long brew: \
	--long match: \
	--long unmatch: \
	--long driver: \
	--long model: \
	--long speed: \
	--long num: \
	--long wb: \
	--long url: \
	--long gov \
	--long test: \
	--long cmd: \
	--long file: \
	--long help \
	-n "$progname" -- "$@")
eval set -- "$_at"

while true; do
	case "$1" in
	-D|--distro)       DISTRO="$2"; shift 2;;
	-B|--brew)         PARAMS+=("--Brew=$2"); shift 2;;
	-d|--driver)       OPTS+=("--driver $2"); shift 2;;
	-m|--model)        OPTS+=("--model $2"); shift 2;;
	-s|--speed)        OPTS+=("--speed $2"); shift 2;;
	-n|--num)          OPTS+=("--num $2"); shift 2;;
	-M|--match)        OPTS+=("--match $2"); shift 2;;
	-U|--unmatch)      OPTS+=("--unmatch $2"); shift 2;;
	-T|--test)         TEST="$2"; shift 2;;
	-C|--cmd)          COMMAND+="$2"; shift 2;;
	-F|--file)         TESTFILE="$2.list"; shift 2;;
	--wb)              PARAMS+=("--wb-comment=$2"); shift 2;;
	--url)             URL="$2"; shift 2;;
	--gov)             GOV="yes"; shift 1;;
	-a)                ALL="yes"; shift 1;;
	-c)                CURRENT="yes"; shift 1;;
	-f)                FORCE="yes"; shift 1;;
	--)                shift; break;;
	-h|--help)         Usage; shift 1; exit 0;;
	esac
done

if [[ -z $DISTRO ]]; then
	echo ":::ERROR::: distro must be exist!"
	exit 1
else
	MAJOR_VER=$(echo "$DISTRO" | awk -F"-" '{print $2}' |  awk -F"." '{print $1}')
	#MINOR_VER=$(echo "$DISTRO" | awk -F"-" '{print $2}' |  awk -F"." '{print $1}')
	if [ "$MAJOR_VER" -le 7 ];then
		PARAMS+=("--variant=Server")
	else
		PARAMS+=("--variant=baseos")
	fi

	if [ "$MAJOR_VER" -ge 9 ]; then
		COMMAND+="update-crypto-policies --set LEGACY;"
	fi
fi

if [[ -n $CURRENT ]]; then
	ORIGIN=$(git remote -v | awk '{print $2}' | head -n1)
	BRANCH=$(git symbolic-ref --short HEAD)
	PARAMS+=("--fetch-url=kernel,$ORIGIN/-/archive/$BRANCH/kernel-master.tar.bz2")
elif [[ -n $URL ]]; then
	PARAMS+=("--fetch-url=$URL")
else
	PARAMS+=("--fetch-url=kernel,https://gitlab.cee.redhat.com/kernel-qe/kernel/-/archive/master/kernel-master.tar.bz2")
fi

if [[ -z $TEST && -z $TESTFILE ]]; then
	echo ":::ERROR::: test case or test file must be exist!"
	exit 1
elif [[ -n $TEST ]]; then
	touch $SH_TASKFILE

	while read -r test; do
		TEST_PATH=$(find "$BASE_PATH" -type d | grep -i "$test" | sort | head -1)
		if [ -z "$TEST_PATH" ]; then
			echo ":::ERROR::: test case $test don't exist!"
			exit 1
		fi

		pushd "$TEST_PATH" > /dev/null 2>&1
		lstest -t 4h >> "$SCRIPT_DIR"/"$SH_TASKFILE"
		popd > /dev/null 2>&1

		if grep -qE "$test.*multiHost" -R "$SCRIPT_DIR"/job_scheduler/list/ || \
		   grep -qE "multiHost" "$SCRIPT_DIR"/"$SH_TASKFILE"; then
			TOPO="multiHost"
		fi
	done <<< "$(echo "$TEST" | tr "," "\n")"

	if ! grep -q "update_via_leapp" $SH_TASKFILE ; then
		PARAMS+=("--kdump=netqe-bj.usersys.redhat.com:/home/kdump/vmcore")
	fi

	[[ $TOPO == "multiHost" ]] && mv $SH_TASKFILE $MH_TASKFILE

elif [[ -n $TESTFILE ]]; then
	if [ ! -f "$SCRIPT_DIR"/job_scheduler/list/"$TESTFILE" ]; then
		echo ":::ERROR::: test file $TESTFILE don't exist!"
		exit 1
	fi

	if grep -q "multiHost" "$SCRIPT_DIR"/job_scheduler/list/"$TESTFILE"; then
		grep "multiHost" "$SCRIPT_DIR"/job_scheduler/list/"$TESTFILE" > "$SCRIPT_DIR"/"$MH_TASKFILE"
	fi
	if grep -qv "multiHost" "$SCRIPT_DIR"/job_scheduler/list/"$TESTFILE"; then
		grep -v "multiHost" "$SCRIPT_DIR"/job_scheduler/list/"$TESTFILE" > "$SCRIPT_DIR"/"$SH_TASKFILE"
	fi

	if ! grep -q "update_via_leapp" "$SCRIPT_DIR"/job_scheduler/list/"$TESTFILE"; then
		PARAMS+=("--kdump=netqe-bj.usersys.redhat.com:/home/kdump/vmcore")
	fi
fi

if [ -n "$COMMAND" ] ; then
	PARAMS+=("--cmd-and-reboot=$COMMAND")
fi

if [[ ${#OPTS[@]} -eq 0 && -z $DEF_MATCH ]]; then
	echo ":::ERROR::: match or driver or model or speed must be exist!"
	exit 1
else
	for opt in "${OPTS[@]}"; do
		KEY=${opt%% *}
		VALUE=${opt##* }

		if [[ $KEY == "--match" ]]; then
			DEF_VALUE="$DEF_MATCH"
			MATCH="yes"
		else
			DEF_VALUE="any"
		fi

		if [[ $VALUE == *","* ]]; then
			SOPTS+=($KEY "$([[ -z "${VALUE%%,*}" ]] && echo "$DEF_VALUE" || echo "${VALUE%%,*}")")
			COPTS+=($KEY "$([[ -z "${VALUE##*,}" ]] && echo "$DEF_VALUE" || echo "${VALUE##*,}")")
		else
			SOPTS+=("$KEY" "$VALUE")
			COPTS+=("$KEY" "$DEF_VALUE")
		fi
	done

	if [[ $MATCH != "yes" ]]; then
		SOPTS+=("--match" "$DEF_MATCH")
		COPTS+=("--match" "$DEF_MATCH")
	fi
fi

if [[ $GOV = "yes" ]]; then
	GOV_START="/kernel/general/code-coverage/start: {attr: {type: Function, level: Tier1, time: 120m, ssched: no, pkg: kernel}}"
	GOV_END="/kernel/general/code-coverage/end: {attr: {type: Function, level: Tier1, time: 60m, ssched: no, pkg: kernel}}"

	if [ -f "$SH_TASKFILE" ]; then
		sed -i "1i\\$GOV_START" $SH_TASKFILE
		sed -i "\$a\\$GOV_END" $SH_TASKFILE
	fi
	if [ -f "$MH_TASKFILE" ]; then
		GOV_START="${GOV_START/%\}/, setup: [--topo=multiHost.1.1]}}"
		GOV_END="${GOV_END/%\}/, setup: [--topo=multiHost.1.1]}}"
		sed -i "1i\\$GOV_START" $MH_TASKFILE
		sed -i "\$a\\$GOV_END" $MH_TASKFILE
	fi
fi

if [ -f "$SH_TASKFILE" ] ; then
	echo "ready to submit signleHost jobs"
	submit_singlehost_job "$@"
fi

if [ -f "$MH_TASKFILE" ] ; then
	PARAMS+=("--topo=multiHost.1.1")
	echo "ready to submit multiHost jobs"
	submit_multihost_job "$@"
fi
