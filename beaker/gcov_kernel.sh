#!/bin/bash
install_dependencies()
{
    if ! which brew &>/dev/null; then
       curl -kLO https://gitlab.cee.redhat.com/kernel-qe/kernel/-/raw/master/networking/common/tools/brewkoji_install.sh
       chmod 777 ./brewkoji_install.sh
       ./brewkoji_install.sh
       rm ./brewkoji_install.sh
    fi
    which createrepo >/dev/null || yum install -y createrepo
    which rsync >/dev/null || yum install -y rsync
    which wget >/dev/null || yum install -y wget
    which sshpass >/dev/null || yum install -y sshpass
}

create_repo()
{
	BREW_TAKS_ID=$1
	if [ -z "$BREW_TAKS_ID" ]; then
		echo "Please input brew task id!"
		exit 1
	fi

	brew download-task "$BREW_TAKS_ID"
	src_rpm=$(find . -type f -name "kernel-*.src.rpm")
	if [ -z "$src_rpm" ]; then
		exit 1
	fi

	kernel_path=$(basename "$src_rpm" | awk -F'.src.rpm' '{print $1}')
	mkdir -p "$kernel_path/noarch"
	mv "$src_rpm" "$kernel_path/noarch"

	for rpm in ./*.rpm; do
		arch=$(rpm -qpi "$rpm" | grep "Architecture" | awk '{print $2}')

		if [ ! -d "$kernel_path/$arch" ]; then
			mkdir -p "$kernel_path/$arch"
		fi

		case "$arch" in
			x86_64|aarch64|ppc64le|i686|s390x|noarch)
				mv "$rpm" "$kernel_path/$arch"
				;;
			*)
				echo "Unknown architecture for $rpm: $arch"
				;;
		esac
	done

	pushd "$kernel_path" || exit 1
	createrepo .
	cat > kernel-gcov.repo <<-EOF
		[$kernel_path]
		name=$kernel_path
		baseurl=http://netqe-bj.usersys.redhat.com/share/gcov-kernels/$kernel_path/
		enabled=1
		gpgcheck=0
	EOF
	popd || exit 1

	rsync -azv "$kernel_path" test@netqe-bj.usersys.redhat.com:/home/share/gcov-kernels
}

create_data()
{
	# kernel-5.14.0-427.32.1.gcov.el9_4.x86_64
	R=$(uname -r)
	RA=$(arch)
	RR=$(uname -r | sed "s/.$RA//")

	URL=http://netqe-bj.usersys.redhat.com/share/gcov-kernels/$RR/$RA
	if ! curl --head --silent --fail "$URL" > /dev/null; then
		echo "Error: $URL not found"
		exit 1
	fi

	# /builddir/build/BUILD/kernel-5.14.0-427.32.1.test.el9_4/linux-5.14.0-427.32.1.gcov.el9_4.x86_64/
	RG=${RR//gcov/test}
	RG=${RR%.*.*}
	BUILD_PATH=$(find /builddir/build/BUILD/ -type d -name "kernel-$RG*" -print -quit)
	if [ ! -d "$BUILD_PATH/linux-$R" ]; then
		echo "Error: source file $RG not found"
		exit 1
	fi

	tar -czf "$R".tar.gz /builddir
	sshpass -p redhat rsync -azv -e "ssh -o StrictHostKeyChecking=no" "$R".tar.gz test@netqe-bj.usersys.redhat.com:/home/share/gcov-kernels/"$RR"/"$RA"
}

install_dependencies

case $1 in
	create_repo)
		create_repo "$2"
		;;
	create_data)
		create_data
		;;
	*)
		echo "gcov_kernel:"
		echo "  $0 create_repo <brewid>                                   Create coverage kernel repo"
		echo "  $0 create_data kernel-5.14.0-427.12.1.gcov.el9_4          Create coverage kenrel srouce file data"
		echo
		exit 1
		;;
esac
