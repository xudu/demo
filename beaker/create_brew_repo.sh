#!/bin/bash
install_packages()
{
    if ! which brew &>/dev/null; then
       curl -kLO https://gitlab.cee.redhat.com/kernel-qe/kernel/-/raw/master/networking/common/tools/brewkoji_install.sh
       chmod 777 ./brewkoji_install.sh
       ./brewkoji_install.sh
       rm ./brewkoji_install.sh
    fi
    which createrepo >/dev/null || yum install -y createrepo
    which sshpass >/dev/null || yum install -y sshpass
}

BREW_TASK_ID=$1
if [ -z "$BREW_TASK_ID" ]; then
    echo "Please input brew task id!"
    exit 1
fi

install_packages

brew download-task "$BREW_TASK_ID"

src_rpm=$(find . -type f -name "kernel-*.src.rpm")
kernel_path=$(basename "$src_rpm" | cut -d'-' -f2- | awk -F'.src.rpm' '{print $1}')
mkdir -p "$kernel_path/noarch"
mv "$src_rpm" "$kernel_path/noarch"

for rpm in ./*.rpm; do
    arch=$(rpm -qpi "$rpm" | grep "Architecture" | awk '{print $2}')

    if [ ! -d "$kernel_path/$arch" ]; then
        mkdir -p "$kernel_path/$arch"
    fi

    case "$arch" in
        x86_64|aarch64|ppc64le|i686|s390x|noarch)
            mv "$rpm" "$kernel_path/$arch"
            ;;
        *)
            echo "Unknown architecture for $rpm: $arch"
            ;;
    esac
done

pushd "$kernel_path" || exit 1
createrepo .
cat > kernel-gcov.repo <<-EOF
[$kernel_path]
name=$kernel_path
baseurl=http://netqe-bj.usersys.redhat.com/share/gcov-kernels/$kernel_path/
enabled=1
gpgcheck=0
EOF
popd || exit 1

sshpass -p xxxxxx rsync -azv -e "ssh -o StrictHostKeyChecking=no" "$kernel_path" test@netqe-bj.usersys.redhat.com:/home/share/gcov-kernels

