#!/bin/sh
COUNT=10

progname=$(basename "$0")
_at=$(getopt -o r:i:h --long help -n "$progname" -- "$@")
eval set -- "$_at"
    while true ; do
    case "$1" in
        -r)
            ROLE=$2; shift 2;;
        -i)
            IFACE=$2; shift 2;;
        --)
            shift; break;;
    esac
done

netns_basictopo()
{
    if [ "$ROLE" = "server" ]; then
        ip link set "$IFACE" up
        ip addr add 2.1.1.1/24 dev "$IFACE"
        ip addr add 2222::1/64 dev "$IFACE"

        ip route add 1.1.1.0/24 via 2.1.1.254 dev "$IFACE"
        ip -6 route add 1111::/64 via 2222::a dev "$IFACE"
        ip route show
        ip -6 route show

        sysctl -w net.ipv6.conf."$IFACE".autoconf=0

    elif [ "$ROLE" = "client" ]; then
        ip netns add client
        ip netns add route
        ip link add dev br1 type bridge

        ip link add veth0_c type veth peer name veth0_c_r
        ip link set veth0_c netns client
        ip link set veth0_c_r netns route
        ip link add veth0_s_b type veth peer name veth0_s_r
        ip link set veth0_s_r netns route
        ip link set dev veth0_s_b master br1
        ip link set dev "$IFACE" master br1

        ip netns exec client ip link set lo up
        ip netns exec client ip link set veth0_c up
        ip netns exec route ip link set lo up
        ip netns exec route ip link set veth0_c_r up
        ip netns exec route ip link set veth0_s_r up
        ip link set br1 up
        ip link set veth0_s_b up
        ip link set "$IFACE" up

        ip netns exec client ip addr add 1.1.1.1/24 dev veth0_c
        ip netns exec client ip addr add 1111::1/64 dev veth0_c
        ip netns exec route ip addr add 1.1.1.254/24 dev veth0_c_r
        ip netns exec route ip addr add 1111::a/64 dev veth0_c_r
        ip netns exec route ip addr add 2.1.1.254/24 dev veth0_s_r
        ip netns exec route ip addr add 2222::a/64 dev veth0_s_r

        ip netns exec route sysctl -w net.ipv6.conf.veth0_c_r.autoconf=0
        ip netns exec route sysctl -w net.ipv6.conf.veth0_s_r.autoconf=0
        ip netns exec client sysctl -w net.ipv6.conf.veth0_c.autoconf=0

        ip netns exec route sysctl -w net.ipv4.conf.all.forwarding=1
        ip netns exec route sysctl -w net.ipv6.conf.all.forwarding=1
        ip netns exec route sysctl -w net.ipv6.conf.all.keep_addr_on_down=1

        sysctl -w net.ipv4.conf.all.forwarding=1
        sysctl -w net.ipv6.conf.all.forwarding=1
        sysctl -w net.ipv6.conf.all.keep_addr_on_down=1

        ip netns exec client ip route add default via 1.1.1.254 dev veth0_c
        ip netns exec client ip -6 route add default via 1111::a dev veth0_c

        ip netns exec route ip link add vrf1 type vrf table 11
        ip netns exec route ip link set vrf1 up
        ip netns exec route ip link set veth0_c_r master vrf1
        ip netns exec route ip link set veth0_s_r master vrf1

        ip netns exec route ip rule list
        ip netns exec route ip route show vrf vrf1
        ip netns exec route ip -6 route show vrf vrf1

        ip netns exec client ping 2.1.1.1 -c 3
        MAC1=$(ip netns exec route ip neigh | grep 2.1.1.1 | awk '{print $5}')
        MAC2=$(ip netns exec route ip link show veth0_s_r | grep ether | awk '{print $2}')
        ebtables -A FORWARD -i "$IFACE" -s "$MAC1" -j ACCEPT
        ebtables -A FORWARD -i "$IFACE" -s "$MAC2" -j ACCEPT
        ebtables -A FORWARD -i "$IFACE" -j DROP
        ebtables -L

    fi
}

netns_cleantopo()
{
    for net in $(ip netns list | awk '{print $1}'); do
        ip netns del "$net"
    done
    modprobe -r veth
    modprobe -r br_netfilter bridge
    ip addr flush "$IFACE"
    ebtables -F
}

PRINT()
{
    if [ "$1" = "PASS" ]; then
        printf "\033[32m%s\033[0m\n" "    $2 $3 PASS"
    else
        printf "\033[31m%s\033[0m\n" "    $2 $3 FAIL"
    fi
}

netns_cleantopo
netns_basictopo

if [ "$ROLE" = "client" ]; then
    rx=$(ip netns exec route ip -s link show vrf1 | sed -n "4p")
    vrf_rx_bytes1=$(echo "$rx" | awk '{print $1}')
    vrf_rx_packets1=$(echo "$rx" | awk '{print $2}')
    vrf_rx_errors1=$(echo "$rx" | awk '{print $3}')
    tx=$(ip netns exec route ip -s link show vrf1 | sed -n "6p")
    vrf_tx_bytes1=$(echo "$tx" | awk '{print $1}')
    vrf_tx_packets1=$(echo "$tx" | awk '{print $2}')
    vrf_tx_errors1=$(echo "$tx" | awk '{print $3}')

    rx=$(ip netns exec client ip -s link show veth0_c | sed -n "4p")
    veth_rx_bytes1=$(echo "$rx" | awk '{print $1}')
    veth_rx_packets1=$(echo "$rx" | awk '{print $2}')
    veth_rx_errors1=$(echo "$rx" | awk '{print $3}')
    tx=$(ip netns exec client ip -s link show veth0_c | sed -n "6p")
    veth_tx_bytes1=$(echo "$tx" | awk '{print $1}')
    veth_tx_packets1=$(echo "$tx" | awk '{print $2}')
    veth_tx_errors1=$(echo "$tx" | awk '{print $3}')

    rx=$(ip -s link show "$IFACE" | sed -n "4p")
    nic_rx_bytes1=$(echo "$rx" | awk '{print $1}')
    nic_rx_packets1=$(echo "$rx" | awk '{print $2}')
    nic_rx_errors1=$(echo "$rx" | awk '{print $3}')
    tx=$(ip -s link show "$IFACE" | sed -n "6p")
    nic_tx_bytes1=$(echo "$tx" | awk '{print $1}')
    nic_tx_packets1=$(echo "$tx" | awk '{print $2}')
    nic_tx_errors1=$(echo "$tx" | awk '{print $3}')

    ip netns exec client ping 2.1.1.1 -c "$COUNT"
    ip netns exec client ping 2222::1 -c "$COUNT"

    echo "---> vrf1 statistics"
    rx=$(ip netns exec route ip -s link show vrf1 | sed -n "4p")
    vrf_rx_bytes2=$(echo "$rx" | awk '{print $1}')
    vrf_rx_packets2=$(echo "$rx" | awk '{print $2}')
    vrf_rx_errors2=$(echo "$rx" | awk '{print $3}')
    tx=$(ip netns exec route ip -s link show vrf1 | sed -n "6p")
    vrf_tx_bytes2=$(echo "$tx" | awk '{print $1}')
    vrf_tx_packets2=$(echo "$tx" | awk '{print $2}')
    vrf_tx_errors2=$(echo "$tx" | awk '{print $3}')

    [ $((vrf_rx_bytes2-vrf_rx_bytes1)) -gt 0 ] && result="PASS" || result="FAIL"
    PRINT "$result" rx_bytes $((vrf_rx_bytes2-vrf_rx_bytes1))
    [ $((vrf_rx_packets2-vrf_rx_packets1)) -ge $((COUNT*4-1)) ] && [ $((vrf_rx_packets2-vrf_rx_packets1)) -le $((COUNT*5)) ] && result="PASS" || result="FAIL"
    PRINT "$result" rx_packets $((vrf_rx_packets2-vrf_rx_packets1))
    [ $((vrf_rx_errors2-vrf_rx_errors1)) -eq 0 ] && result="PASS" || result="FAIL"
    PRINT "$result" rx_errors $((vrf_rx_errors2-vrf_rx_errors1))
    echo ""
    [ $((vrf_tx_bytes2-vrf_tx_bytes1)) -eq 0 ] && result="PASS" || result="FAIL"
    PRINT "$result" tx_bytes $((vrf_tx_bytes2-vrf_tx_bytes1))
    [ $((vrf_tx_packets2-vrf_tx_packets1)) -eq 0 ] && result="PASS" || result="FAIL"
    PRINT "$result" tx_packets $((vrf_tx_packets2-vrf_tx_packets1))
    [ $((vrf_tx_errors2-vrf_tx_errors1)) -eq 0 ] && result="PASS" || result="FAIL"
    PRINT "$result" tx_errors $((vrf_tx_errors2-vrf_tx_errors1))

    echo ""
    echo ""
    echo "---> veth0_c statistics"
    rx=$(ip netns exec client ip -s link show veth0_c | sed -n "4p")
    veth_rx_bytes2=$(echo "$rx" | awk '{print $1}')
    veth_rx_packets2=$(echo "$rx" | awk '{print $2}')
    veth_rx_errors2=$(echo "$rx" | awk '{print $3}')
    tx=$(ip netns exec client ip -s link show veth0_c | sed -n "6p")
    veth_tx_bytes2=$(echo "$tx" | awk '{print $1}')
    veth_tx_packets2=$(echo "$tx" | awk '{print $2}')
    veth_tx_errors2=$(echo "$tx" | awk '{print $3}')

    [ $((veth_rx_bytes2-veth_rx_bytes1)) -gt 0 ] && result="PASS" || result="FAIL"
    PRINT "$result" rx_bytes $((veth_rx_bytes2-veth_rx_bytes1))
    [ $((veth_rx_packets2-veth_rx_packets1)) -ge $((COUNT*2)) ] && [ $((veth_rx_packets2-veth_rx_packets1)) -le $((COUNT*3)) ] && result="PASS" || result="FAIL"
    PRINT "$result" rx_packets $((veth_rx_packets2-veth_rx_packets1))
    [ $((veth_rx_errors2-veth_rx_errors1)) -eq 0 ] && result="PASS" || result="FAIL"
    PRINT "$result" rx_errors $((veth_rx_errors2-veth_rx_errors1))
    echo ""
    [ $((veth_tx_bytes2-veth_tx_bytes1)) -gt 0 ] && result="PASS" || result="FAIL"
    PRINT "$result" tx_bytes $((veth_tx_bytes2-veth_tx_bytes1))
    [ $((veth_tx_packets2-veth_tx_packets1)) -ge $((COUNT*2)) ] && [ $((veth_tx_packets2-veth_tx_packets1)) -le $((COUNT*3)) ] && result="PASS" || result="FAIL"
    PRINT "$result" veth_tx_packets $((veth_tx_packets2-veth_tx_packets1))
    [ $((veth_tx_errors2-veth_tx_errors1)) -eq 0 ] && result="PASS" || result="FAIL"
    PRINT "$result" tx_errors $((veth_tx_errors2-veth_tx_errors1))

    echo ""
    echo ""
    echo "---> $IFACE statistics"
    rx=$(ip -s link show "$IFACE" | sed -n "4p")
    nic_rx_bytes2=$(echo "$rx" | awk '{print $1}')
    nic_rx_packets2=$(echo "$rx" | awk '{print $2}')
    nic_rx_errors2=$(echo "$rx" | awk '{print $3}')
    tx=$(ip -s link show "$IFACE" | sed -n "6p")
    nic_tx_bytes2=$(echo "$tx" | awk '{print $1}')
    nic_tx_packets2=$(echo "$tx" | awk '{print $2}')
    nic_tx_errors2=$(echo "$tx" | awk '{print $3}')

    [ $((nic_rx_bytes2-nic_rx_bytes1)) -gt 0 ] && result="PASS" || result="FAIL"
    PRINT "$result" rx_bytes $((nic_rx_bytes2-nic_rx_bytes1))
    [ $((nic_rx_packets2-nic_rx_packets1)) -ge $((COUNT*2)) ] && result="PASS" || result="FAIL"
    PRINT "$result" rx_packets $((nic_rx_packets2-nic_rx_packets1))
    [ $((nic_rx_errors2-nic_rx_errors1)) -eq 0 ] && result="PASS" || result="FAIL"
    PRINT "$result" rx_errors $((nic_rx_errors2-nic_rx_errors1))
    echo ""
    [ $((nic_tx_bytes2-nic_tx_bytes1)) -gt 0 ] && result="PASS" || result="FAIL"
    PRINT "$result" tx_bytes $((nic_tx_bytes2-nic_tx_bytes1))
    [ $((nic_tx_packets2-nic_tx_packets1)) -ge $((COUNT*2)) ] && result="PASS" || result="FAIL"
    PRINT "$result" tx_packets $((nic_tx_packets2-nic_tx_packets1))
    [ $((nic_tx_errors2-nic_tx_errors1)) -eq 0 ] && result="PASS" || result="FAIL"
    PRINT "$result" tx_errors $((nic_tx_errors2-nic_tx_errors1))
fi