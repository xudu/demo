#!/usr/bin/python
from __future__ import print_function

import os
import sys
import time
import signal
import socket
import argparse
import traceback
import logging

from logging import handlers

logger = logging.getLogger()
log_path = "/var/log/netsync/sync.log"


def retry(times=3, seconds=1):
    def decorator(func):
        def wrapper(*args, **kwargs):
            i = 0
            result = func(*args, **kwargs)
            while result != 0 and i < times:
                time.sleep(seconds)
                i += 1
                result = func(*args, **kwargs)
            return result

        return wrapper

    return decorator


class NetSync(object):
    def __init__(self, peer, port, key):
        self._hostname = socket.gethostname()
        self._peername = peer
        self._port = int(port)
        self._key = key
        self.msg_len = 1024
        self.max_lis = 20

    def _startserver(self):
        ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ss.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        ss.bind(("0.0.0.0", self._port))
        ss.listen(self.max_lis)
        logger.info(
            f"{self._hostname} wait {self._key} from {self._peername}:{self._port}"
        )

        while True:
            cs, caddr = ss.accept()
            logger.debug(
                f"{self._hostname} accepted {self._key} from {self._peername}:{self._port}"
            )

            try:
                data = cs.recv(self.msg_len)
                message = data.decode("utf-8")
                paddr = socket.gethostbyname(self._peername)
                logger.info(
                    f"{self._hostname} recevie {message} from {self._peername}"
                )
                if message == self._key and caddr[0] == paddr:
                    print(message)
                    response = "ok"
                    cs.sendall(response.encode("utf-8"))
                    break
                else:
                    response = "error"
                    cs.sendall(response.encode("utf-8"))
                    logger.info(
                        f"{self._hostname} recevie {cs[0]}:{message} don't match {paddr}:{self._key}"
                    )
            except Exception as e:
                logger.error(
                    f"{self._hostname} communicating with {self._peername}: {e}"
                )
            finally:
                cs.close()

        ss.close()
        return None

    @retry(times=6, seconds=10)
    def _startclient(self):
        res = -1
        cs = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        cs.settimeout(10)

        try:
            paddr = socket.gethostbyname(self._peername)
            cs.connect((paddr, self._port))
            cs.sendall(self._key.encode("utf-8"))
            data = cs.recv(self.msg_len)
            response = data.decode("utf-8")
            logger.info(
                f"{self._hostname} set {self._key} to {self._peername}:{self._port} {response}"
            )
            if response == "ok":
                res = 0
        except Exception as e:
            logger.debug(
                f"{self._hostname} communicating with the {self._peername}:{e}"
            )
        finally:
            cs.close()
        return res

    def run(self, action):
        return self._startserver() if action == "wait" else self._startclient()


def signal_handler(sig, frame):
    sys.stdout.write("Caught signal SIGUSR1, printing traceback:")
    sys.stdout.flush()
    traceback.print_stack(frame)
    sys.exit(1)


def init_logger():
    base_dir = os.path.dirname(log_path)
    if not os.path.exists(base_dir):
        os.makedirs(base_dir)

    th = handlers.TimedRotatingFileHandler(log_path, when="H", interval=1)
    th.setFormatter(
        logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    )
    logger.setLevel(logging.DEBUG)
    logger.addHandler(th)


def process_options():
    def validate_port(value):
        if not 1023 < int(value) < 65536:
            raise argparse.ArgumentTypeError(
                f"Port must be in the range 1-65535, got {value}"
            )
        return value

    def validate_action(set, wait):
        if set and wait:
            raise argparse.ArgumentTypeError(f"Either set {set} or wait {wait}")
        return True

    parser = argparse.ArgumentParser(
        usage="""

        Examples:
        ---------

        To set peer servers a key string for synchronization control
        net_sync.py --set test -H dell-per750-53.rhts.eng.pek2.redhat.com -P 54321

        To wait peer servers a key string for synchronization control
        net_sync.py --wait test -H dell-per750-53.rhts.eng.pek2.redhat.com -P 54321

        """,
        description=" net control synchronization ",
    )
    parser.add_argument("--set", type=str, help=""" Set key string to peers """)
    parser.add_argument(
        "--wait", type=str, help=""" Wait key string from peers """
    )
    parser.add_argument(
        "-H", "--host", required=True, help="Host name or IP address"
    )
    parser.add_argument(
        "-P", "--port", required=True, type=validate_port, help="Port number"
    )

    args = parser.parse_args()
    validate_action(args.set, args.wait)
    return args


def main():
    try:
        signal.signal(signal.SIGUSR1, signal_handler)
        init_logger()

        options = process_options()
        if options.set:
            action = "set"
            key = options.set
        else:
            action = "wait"
            key = options.wait
        logger.info(f"{action} {options.host} {key}")

        sync = NetSync(options.host, options.port, key)
        rc = sync.run(action)
    except Exception:
        traceback.print_exc()
        sys.exit(-1)
    except KeyboardInterrupt:
        logger.error("Ctrl+C")
        sys.exit(-1)
    return rc


if __name__ == "__main__":
    main()
